<!-- Begin Content -->
	<section class="content special" data-wow-delay="0.5s">
		<div class="row collapse">
			<div class="small-12 medium-3 columns post_menu">
				<?php
				wp_nav_menu(
					array(
						'menu_class' => 'vertical menu',
						'container' => false,
						'theme_location' => 'atracciones-menu',
						'items_wrap' => '<ul class="%2$s">%3$s</ul>'
					)
				);
				?>
			</div>
			<div class="small-12 medium-9 columns post_main">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<div class="post_thumbnail"><?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?></div>
					<div class="post_content">
						<?php the_title( '<h1>', '</h1>' ); ?>
						<?php the_content(); ?>
					</div>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</section>
<!-- End Content -->