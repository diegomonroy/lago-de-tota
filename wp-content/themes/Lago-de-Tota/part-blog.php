<!-- Begin Content -->
	<section class="content special" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 medium-9 columns category_main">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
						<?php if ( is_category( 'actualidad-lagunera' ) ) : ?>
						<p class="text-center"><?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?></p>
						<?php endif; ?>
						<?php the_excerpt(); ?>
						<p class="text-center"><a href="<?php the_permalink(); ?>" class="hollow button">Leer más...</a></p>
					</article>
				<?php endwhile; endif; ?>
			</div>
			<div class="small-12 medium-3 columns">
				<?php if ( is_category( 'actualidad-lagunera' ) ) : ?>
					<?php dynamic_sidebar( 'right_actualidad_lagunera' ); ?>
				<?php elseif ( is_category( 'blog' ) ) : ?>
					<?php dynamic_sidebar( 'right_blog' ); ?>
				<?php else : ?>
					<?php dynamic_sidebar( 'right' ); ?>
				<?php endif; ?>
			</div>
		</div>
	</section>
<!-- End Content -->