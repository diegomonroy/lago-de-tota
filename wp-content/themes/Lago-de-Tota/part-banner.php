<!-- Begin Banner -->
	<section class="banner" data-wow-delay="0.5s">
		<div class="row collapse expanded">
			<div class="small-12 columns">
				<?php if ( is_front_page() ) : dynamic_sidebar( 'banner_inicio' ); endif; ?>
				<?php if ( is_page( array( 'nosotros' ) ) ) : dynamic_sidebar( 'banner_nosotros' ); endif; ?>
				<?php if ( is_page( array( 'sostenibilidad' ) ) ) : dynamic_sidebar( 'banner_sostenibilidad' ); endif; ?>
			</div>
		</div>
	</section>
<!-- End Banner -->