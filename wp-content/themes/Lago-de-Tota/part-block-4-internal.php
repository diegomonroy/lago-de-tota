<!-- Begin Block 4 -->
	<section class="block_4" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'block_4_internal' ); ?>
			</div>
		</div>
	</section>
<!-- End Block 4 -->