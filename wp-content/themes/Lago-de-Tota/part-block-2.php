<!-- Begin Block 2 -->
	<section class="block_2" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'block_2' ); ?>
			</div>
		</div>
		<div class="row">
			<div class="small-12 medium-6 columns">
				<?php dynamic_sidebar( 'block_2_1' ); ?>
			</div>
			<div class="small-12 medium-6 columns">
				<?php dynamic_sidebar( 'block_2_2' ); ?>
			</div>
		</div>
	</section>
<!-- End Block 2 -->