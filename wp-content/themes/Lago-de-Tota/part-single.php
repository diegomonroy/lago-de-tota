<?php

$categories = get_the_category();
$category = esc_html( $categories[0]->slug );

?>
<!-- Begin Content -->
	<section class="content special" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 medium-9 columns single_main">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php if ( $category == 'actualidad-lagunera' ) : ?>
					<div class="single_thumbnail text-center"><?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?></div>
					<?php endif; ?>
					<div class="single_content">
						<?php the_title( '<h1>', '</h1>' ); ?>
						<?php the_content(); ?>
					</div>
				<?php endwhile; endif; ?>
			</div>
			<div class="small-12 medium-3 columns">
				<?php if ( $category == 'actualidad-lagunera' ) : ?>
					<?php dynamic_sidebar( 'right_actualidad_lagunera' ); ?>
				<?php elseif ( $category == 'blog' ) : ?>
					<?php dynamic_sidebar( 'right_blog' ); ?>
				<?php else : ?>
					<?php dynamic_sidebar( 'right' ); ?>
				<?php endif; ?>
			</div>
		</div>
	</section>
<!-- End Content -->